// JMSGETDELETEMESSAGE - Retrieve & Delete Message from MQ Server
package com.vantec.jms;
// import org.apache.activemq.ActiveMQConnectionFactory; 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import com.ibm.mq.jms.MQConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;

/**
 ****************************************************************************************************
 * A JMS queue consumer application that receives & deletes messages
 * in the order they would be received by a consumer application.
 * 
 * The Received Messages are written to a text file (default is JMSDataOut.TXT)
 *
 * Messages can be retrieved from IBM Websphere MQS or ActiveMQ Servers
 *
 * For MQS a valid Queue Manager Name must be specified, for ActiveMQ the Queue Manager must be "AMQ"
 *
 * If Host is not specified "localhost" is assumed with the appropriate port for MQS or AMQ
 *
 ****************************************************************************************************
 * Command Line Usage:
 * 
 * JmsGetDeleteMessage -m queueManagerName ('AMQ'=ActiveMQ) -d queueName [-h host -p port -l channel -f filename]
 * 
 * for example:
 * 
 * JmsGetDeleteMessage -m QM1 -d Q1 
 * 
 * JmsGetDeleteMessage -m QM1 -d Q1 -h localhost -p 1414
 *
 * JmsGetDeleteMessage -m AMQ -d Q1 -h localhost -p 61616
 *
 * JmsGetDeleteMessage -m QM1 -d Q1 -h localhost -p 1414 -f "c:\mqs\messages.txt"
 *
 * JmsGetDeleteMessage -m AMQ -d Q1 -h localhost -p 61616 -f "c:\amq\messages.txt"
 *
 ****************************************************************************************************
 */
public class JmsGetDeleteMessage {

  private static String host = "localhost";
  private static int port = -1;
  private static String channel = "SYSTEM.DEF.SVRCONN";
  private static String queueManagerName = null;
  private static String queueName = null;
  private static String fileName = null;
  private static String destinationName = null;
  private static boolean isTopic = false;

  // MQS Connection Timout Value (ms) 10000 = 10 seconds
  private static int timeout = 10000;
  // System exit status value (assume unset value to be 1)
  private static int status = 1;

  /**
   * Main method
   * 
   * @param args
   */
  public static void main(String[] args) {
    // Parse the arguments
//-m VIMSRRI10 -d VIMS.LSP_ASN.BMW -h 172.18.21.5 -f "/home/vimadmin/jms/jmsdataout.txt"
    parseArgs(args);

    // Variables
    Connection connection = null;
    Session session = null;
	Destination destination = null;
    MessageConsumer consumer = null;

    try {
      // Create a connection factory

	  if (queueManagerName == "AMQ")
	  {
//		ActiveMQConnectionFactory amcf = new ActiveMQConnectionFactory("tcp://"+host+":"+port);
//		connection = amcf.createConnection();
	  }
	  else
	  {
		  MQConnectionFactory  mqcf = new MQConnectionFactory();
		mqcf.setHostName(host);                          		// server IP address / name
		mqcf.setPort(port);
		mqcf.setTransportType(WMQConstants.WMQ_CM_DIRECT_TCPIP);   // for TCP/IP  JMSC.MQJMS_TP_CLIENT_MQ_TCPIP
		mqcf.setChannel(channel);                          		// server connection channel to use
		mqcf.setQueueManager(queueManagerName);               	// queue manager to use
		connection = mqcf.createConnection();
	  }

      // Create JMS objects
      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      destination = session.createQueue(queueName);
	  consumer = session.createConsumer(destination);

      // Start the connection
      connection.start();

	  // Open File for Writing Message data 
	File wf= new File(fileName);

           
	  
	  // And, receive the first message
      Message current = consumer.receive(timeout);
      
      int count = 0;
	  
	  // echo to Command Line Window
	  while (current != null)
	  {
        System.out.println("\nMessage " + ++count + ":\n");
        System.out.println(current);
		
	 // echo to Text File
	try{
		BufferedWriter output = new BufferedWriter(new FileWriter(wf));
		 output.write(current +"\r\n");
	}catch(IOException io){
	}
		
	// And, receive the next message
        current = consumer.receive(timeout);
      }
	  
      System.out.println("\nNo more messages\n");
	  
      recordSuccess();
    }
    catch (JMSException jmsex) {
      recordFailure(jmsex);
    }
    finally {
      if (consumer != null) {
        try {
          consumer.close();
        }
        catch (JMSException jmsex) {
          System.out.println("Consumer could not be closed.");
          recordFailure(jmsex);
        }
      }

      if (session != null) {
        try {
          session.close();
        }
        catch (JMSException jmsex) {
          System.out.println("Session could not be closed.");
          recordFailure(jmsex);
        }
      }

      if (connection != null) {
        try {
          connection.close();
        }
        catch (JMSException jmsex) {
          System.out.println("Connection could not be closed.");
          recordFailure(jmsex);
        }
      }
    }
    System.exit(status);
    return;
  } // end main()

  /**
   * Process a JMSException and any associated inner exceptions.
   * 
   * @param jmsex
   */
  private static void processJMSException(JMSException jmsex) {
    System.out.println(jmsex);
    Throwable innerException = jmsex.getLinkedException();
    if (innerException != null) {
      System.out.println("Inner exception(s):");
    }
    while (innerException != null) {
      System.out.println(innerException);
      innerException = innerException.getCause();
    }
    return;
  }

  /**
   * Record this run as successful.
   */
  private static void recordSuccess() {
    System.out.println("SUCCESS");
    status = 0;
    return;
  }

  /**
   * Record this run as failure.
   * 
   * @param ex
   */
  private static void recordFailure(Exception ex) {
    if (ex != null) {
      if (ex instanceof JMSException) {
        processJMSException((JMSException) ex);
      }
      else {
        System.out.println(ex);
      }
    }
    System.out.println("FAILURE");
    status = -1;
    return;
  }

  /**
   * Parse user supplied arguments.
   * 
   * @param args
   */
  private static void parseArgs(String[] args) {
    try {
      int length = args.length;
      if (length == 0) {
        throw new IllegalArgumentException("No arguments! Mandatory arguments must be specified.");
      }
	  
//      if ((length % 2) != 0) {
//        throw new IllegalArgumentException("Incorrect number of arguments!");
//      }

      int i = 0;

      while (i < length) {
        if ((args[i]).charAt(0) != '-') {
          throw new IllegalArgumentException("Expected a '-' character next: " + args[i]);
        }

        char opt = (args[i]).toLowerCase().charAt(1);

        switch (opt) {
          case 'h' :
            host = args[++i];
            break;
          case 'p' :
            port = Integer.parseInt(args[++i]);
            break;
          case 'l' :
            channel = args[++i];
            break;
          case 'm' :
            queueManagerName = args[++i];
            break;
          case 'd' :
            queueName = args[++i];
            break;
		  case 'f' :
            fileName = args[++i];
            break;
          default : 
            System.out.println ("Unknown argument: " + opt);
          
        }

        ++i;
      }

	  if ((queueManagerName.toLowerCase() == "amq") && (port == -1))
	  {
		port = 61616;
	  }
		
	  if (port == -1)
		port = 1414;
		
	  if ((fileName == null) || (fileName == ""))
	  {
		fileName = "JMSDataOut.TXT";
	  }
	  boolean fileSuccessfullyDeleted =  new File(fileName).delete();
	  
      if (queueManagerName == null) {
        throw new IllegalArgumentException("A queueManager name must be specified.");
      }

      if (queueName == null) {
        throw new IllegalArgumentException("A queue name must be specified.");
      }
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      printUsage();
      System.exit(-1);
    }
    return;
  }

  /**
   * Display usage help.
   */
  private static void printUsage() {
    System.out.println("\nUsage:");
    System.out.println("JmsGetDeleteMessage -m queueManagerName -d queueName [-h host -p port -l channel -f fileNmae]");
    return;
  }

} // end class