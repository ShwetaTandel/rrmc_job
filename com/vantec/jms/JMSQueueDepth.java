package com.vantec.jms;

import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;

public class JMSQueueDepth {

	private static String host = "localhost";
	private static int port = -1;
	private static String channel = "SYSTEM.DEF.SVRCONN";
	private static String queueManagerName = null;
	private static String queueName = null;
	private static MQQueue destQueue;

	private static int openOptions = CMQC.MQOO_INQUIRE + CMQC.MQOO_FAIL_IF_QUIESCING + CMQC.MQOO_BROWSE;
	private static int status = 0;

	public static void checkQueue(String[] args) {

		parseArgs(args);

		MQEnvironment.hostname = host;
		// MQEnvironment.hostname = "172.18.21.5";
		MQEnvironment.port = port;
		// MQEnvironment.port = 1414;
		MQEnvironment.channel = channel;

		MQQueueManager qMgr;
		try {
			qMgr = new MQQueueManager(queueManagerName);
		
			destQueue = qMgr.accessQueue(queueName, openOptions);
			System.out.println("Queue Depth:" + destQueue.getCurrentDepth());
			status = destQueue.getCurrentDepth();
			destQueue.close();
			qMgr.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.exit(status);
		return;
	}

	/**
	 * Parse user supplied arguments.
	 * 
	 * @param args
	 */
	private static void parseArgs(String[] args) {
		try {
			int length = args.length;
			if (length == 0) {
				throw new IllegalArgumentException("No arguments! Mandatory arguments must be specified.");
			}

			int i = 0;

			while (i < length) {
				if ((args[i]).charAt(0) != '-') {
					throw new IllegalArgumentException("Expected a '-' character next: " + args[i]);
				}

				char opt = (args[i]).toLowerCase().charAt(1);

				switch (opt) {
				case 'h' :
					host = args[++i];
					break;
				case 'p' :
					port = Integer.parseInt(args[++i]);
					break;
				case 'l' :
					channel = args[++i];
					break;
				case 'm' :
					queueManagerName = args[++i];
					break;
				case 'd' :
					queueName = args[++i];
					break;
				default :System.out.println ("Unknown argument: " + opt);
				}
				++i;
			}

			if ((queueManagerName.toLowerCase().equals("amq")) && (port == -1)){
				port = 61616;
			}

			if (port == -1){
				port = 1414;
			}

			if (queueManagerName == null) {
				throw new IllegalArgumentException("A queueManager name must be specified.");
			}

			if (queueName == null){
				throw new IllegalArgumentException("A queue name must be specified.");
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			printUsage();
			System.exit(-1);
		}
		return;
	}

	/**
	 * Display usage help.
	 */
	private static void printUsage() {
		System.out.println("\nUsage:");
		System.out.println("JmsGetMessageCount -m queueManagerName -d queueName [-h host -p port -l channel");
		return;
	}
}
